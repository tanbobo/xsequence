package com.xuanner.seq;

import com.xuanner.seq.sequence.Sequence;

/**
 * 构建序列号生成结构
 * Created by xuan on 2018/5/30.
 */
public interface SeqBuilder {

    Sequence build();
}
