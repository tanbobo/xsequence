package com.xuanner.seq;

import com.xuanner.seq.range.impl.redis.RedisSeqRangeMgr;
import com.xuanner.seq.sequence.Sequence;
import com.xuanner.seq.sequence.impl.DefaultRangeSequence;

/**
 * 基于redis取步长序列号生成器构建
 * Created by xuan on 2018/5/30.
 */
public class RedisSeqBuilder implements SeqBuilder {

    /**
     * ip[必选]
     */
    private String ip;
    /**
     * port[必选]
     */
    private int    port;
    /**
     * 认证权限[可选]
     */
    private String auth;
    /**
     * 获取range步长[可选，默认：1000]
     */
    private int    step = 1000;
    /**
     * 业务名称[必选]
     */
    private String bizName;

    @Override
    public Sequence build() {
        //利用Redis获取区间管理器
        RedisSeqRangeMgr redisSeqRangeMgr = new RedisSeqRangeMgr();
        redisSeqRangeMgr.setIp(this.ip);//IP[必选]
        redisSeqRangeMgr.setPort(this.port);//PORT[必选]
        redisSeqRangeMgr.setAuth(this.auth);//密码[可选]看你的redis服务端配置是否需要密码
        redisSeqRangeMgr.setStep(this.step);//每次取数步长[可选] 默认：1000
        redisSeqRangeMgr.init();
        //构建序列号生成器
        DefaultRangeSequence sequence = new DefaultRangeSequence();
        sequence.setName(this.bizName);
        sequence.setSeqRangeMgr(redisSeqRangeMgr);
        return sequence;
    }

    public static RedisSeqBuilder create() {
        RedisSeqBuilder builder = new RedisSeqBuilder();
        return builder;
    }

    public RedisSeqBuilder ip(String ip) {
        this.ip = ip;
        return this;
    }

    public RedisSeqBuilder port(int port) {
        this.port = port;
        return this;
    }

    public RedisSeqBuilder auth(String auth) {
        this.auth = auth;
        return this;
    }

    public RedisSeqBuilder step(int step) {
        this.step = step;
        return this;
    }

    public RedisSeqBuilder bizName(String bizName) {
        this.bizName = bizName;
        return this;
    }

}
