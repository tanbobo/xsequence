package com.xuanner.seq;

import com.xuanner.seq.sequence.Sequence;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/**
 * Created by xuan on 2018/1/10.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:db-test.xml" })
public class DbTest_Spring {

    @Autowired
    private Sequence userSeq;

    @Test
    public void test() {
        for (int i = 0; i < 1000; i++) {
            System.out.println("++++++++++id:" + userSeq.nextValue());
        }
    }

}
